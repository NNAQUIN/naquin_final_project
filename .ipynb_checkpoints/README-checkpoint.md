# Winter Storm Kenan

This notebook offers an analysis of Winter Storm Kenan through various weather meteorological data sets and types.

## Description

Winter Storm Kenan hit the Northeastern United States in late January of 2022 when an upper-level disturbance over the plains propagated eastward and met with a weak low pressure system over the coast of Florida, causing major snowfall and blizzard conditions for many in the Mid Atlantic and New England states. The cyclone udnerwent "bombogenesis", a term coined for low pressure systems that drop at least 24 millibars in 24 hours. Winter Storm Kenan dropped 36 millibars in just 18 hours, as well as well over a foot of snow across the Northeast, peaking at 30 inches in Massachusetts. Most of my data will be from the Boston area (or Massachusetts in general), since the Massachusetts coast received the most snowfall. In this notebook, I will be exploring various aspects of the storm using satellite imagery, RADAR data, sounding data, and ASOS data.

## Getting Started

### Dependencies

### Executing program

This notebook was created under the AOS573_Tutorials environment, details of which can be found under the YML file for the environment.

The notebook requires the following packages:

   NumPy
   SciPy
   XArray
   SatPy
   goes2go
   MatPlotLib
   PyART
   Folium
   WindRose
   CartoPy

## Authors

Natalie Naquin

## Version History

* 0.2
    * Various bug fixes and optimizations
    * See [commit change]() or See [release history]()
* 0.1
    * Initial Release


## Acknowledgments
